package com.nexusinteractivegroup.va_memorypalace;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BootReceiver extends BroadcastReceiver
{
	
	@Override
	public void onReceive(Context arg0, Intent arg1)
	{
		
		//Toast.makeText(arg0, "BOOT COMPLETED broadcast received. Executing starter service.", Toast.LENGTH_LONG).show();

    	//Intent serviceLauncher = new Intent(arg0, BootService.class);   	 
   	 	//arg0.startService(serviceLauncher);
		

		Intent StartIntent = new Intent(arg0,Startup.class);
		StartIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				
		arg0.startActivity(StartIntent);
		
	}

}
