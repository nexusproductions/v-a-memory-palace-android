package com.nexusinteractivegroup.va_memorypalace.animation;

public interface OnAnimationStoppedListener
{
	public void AnimationStopped();
}
