package com.nexusinteractivegroup.va_memorypalace.animation;

import java.lang.ref.SoftReference;

import com.nexusinteractivegroup.va_memorypalace.R;
import com.nexusinteractivegroup.va_memorypalace.DrawView.SVGInterface;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.widget.ImageView;

public class AnimationsContainer 
{
	
	static public AnimInterface onAnimChange;
	
	public interface AnimInterface 
	{
		public void onFinish();
		
	}
	
	public void setAnimInterface(AnimInterface listener) 
	{
		onAnimChange = listener;
	}
	
	
    public int FPS = 25;  // animation FPS

    // single instance procedures
    private static AnimationsContainer mInstance;

    private AnimationsContainer() 
    {
    	
    };

    public static AnimationsContainer getInstance() 
    {
        if (mInstance == null)
            mInstance = new AnimationsContainer();
        return mInstance;
    }

    // animation progress dialog frames
    private int[] mProgressAnimFrames = { 	R.drawable.ds_fullscreen_entire_00001, R.drawable.ds_fullscreen_entire_00002,R.drawable.ds_fullscreen_entire_00003,
											R.drawable.ds_fullscreen_entire_00004, R.drawable.ds_fullscreen_entire_00005,R.drawable.ds_fullscreen_entire_00006,
											R.drawable.ds_fullscreen_entire_00007, R.drawable.ds_fullscreen_entire_00008,R.drawable.ds_fullscreen_entire_00009,
											R.drawable.ds_fullscreen_entire_00010, R.drawable.ds_fullscreen_entire_00011,R.drawable.ds_fullscreen_entire_00012,
											R.drawable.ds_fullscreen_entire_00013, R.drawable.ds_fullscreen_entire_00014,R.drawable.ds_fullscreen_entire_00015,
											R.drawable.ds_fullscreen_entire_00016, R.drawable.ds_fullscreen_entire_00017,R.drawable.ds_fullscreen_entire_00018,
											R.drawable.ds_fullscreen_entire_00019, R.drawable.ds_fullscreen_entire_00020,R.drawable.ds_fullscreen_entire_00021,
											R.drawable.ds_fullscreen_entire_00022, R.drawable.ds_fullscreen_entire_00023,R.drawable.ds_fullscreen_entire_00024,
											R.drawable.ds_fullscreen_entire_00025, R.drawable.ds_fullscreen_entire_00026,R.drawable.ds_fullscreen_entire_00027,
											R.drawable.ds_fullscreen_entire_00028, R.drawable.ds_fullscreen_entire_00029,R.drawable.ds_fullscreen_entire_00030,
											R.drawable.ds_fullscreen_entire_00031, R.drawable.ds_fullscreen_entire_00032,R.drawable.ds_fullscreen_entire_00033,
											R.drawable.ds_fullscreen_entire_00034, R.drawable.ds_fullscreen_entire_00035,R.drawable.ds_fullscreen_entire_00036,
											R.drawable.ds_fullscreen_entire_00037, R.drawable.ds_fullscreen_entire_00038,R.drawable.ds_fullscreen_entire_00039,
											R.drawable.ds_fullscreen_entire_00040, R.drawable.ds_fullscreen_entire_00041,R.drawable.ds_fullscreen_entire_00042,
											R.drawable.ds_fullscreen_entire_00043, R.drawable.ds_fullscreen_entire_00044,R.drawable.ds_fullscreen_entire_00045,
											R.drawable.ds_fullscreen_entire_00046, R.drawable.ds_fullscreen_entire_00047,R.drawable.ds_fullscreen_entire_00048,
											R.drawable.ds_fullscreen_entire_00059, R.drawable.ds_fullscreen_entire_00050,R.drawable.ds_fullscreen_entire_00051,
											R.drawable.ds_fullscreen_entire_00052, R.drawable.ds_fullscreen_entire_00053,R.drawable.ds_fullscreen_entire_00054,
											R.drawable.ds_fullscreen_entire_00055, R.drawable.ds_fullscreen_entire_00056,R.drawable.ds_fullscreen_entire_00057,
											R.drawable.ds_fullscreen_entire_00058, R.drawable.ds_fullscreen_entire_00059,R.drawable.ds_fullscreen_entire_00060,
    										R.drawable.ds_fullscreen_entire_00061,R.drawable.ds_fullscreen_entire_00062,
								    		R.drawable.ds_fullscreen_entire_00063, R.drawable.ds_fullscreen_entire_00064,R.drawable.ds_fullscreen_entire_00065,
								    		R.drawable.ds_fullscreen_entire_00066, R.drawable.ds_fullscreen_entire_00067,R.drawable.ds_fullscreen_entire_00068,
								    		R.drawable.ds_fullscreen_entire_00069, R.drawable.ds_fullscreen_entire_00070,R.drawable.ds_fullscreen_entire_00071,
								    		R.drawable.ds_fullscreen_entire_00072, R.drawable.ds_fullscreen_entire_00073,R.drawable.ds_fullscreen_entire_00074,
								    		R.drawable.ds_fullscreen_entire_00075, R.drawable.ds_fullscreen_entire_00076,R.drawable.ds_fullscreen_entire_00077,
								    		R.drawable.ds_fullscreen_entire_00078, R.drawable.ds_fullscreen_entire_00079,R.drawable.ds_fullscreen_entire_00080,
								    		R.drawable.ds_fullscreen_entire_00081, R.drawable.ds_fullscreen_entire_00082,R.drawable.ds_fullscreen_entire_00083,
								    		R.drawable.ds_fullscreen_entire_00084, R.drawable.ds_fullscreen_entire_00085,R.drawable.ds_fullscreen_entire_00086,
								    		R.drawable.ds_fullscreen_entire_00087, R.drawable.ds_fullscreen_entire_00088,R.drawable.ds_fullscreen_entire_00089,
								    		R.drawable.ds_fullscreen_entire_00090, R.drawable.ds_fullscreen_entire_00091,R.drawable.ds_fullscreen_entire_00092,
								    		R.drawable.ds_fullscreen_entire_00093, R.drawable.ds_fullscreen_entire_00094,R.drawable.ds_fullscreen_entire_00095,
								    		R.drawable.ds_fullscreen_entire_00096, R.drawable.ds_fullscreen_entire_00097,R.drawable.ds_fullscreen_entire_00098,
								    		R.drawable.ds_fullscreen_entire_00099, R.drawable.ds_fullscreen_entire_00100,R.drawable.ds_fullscreen_entire_00101,
								    		R.drawable.ds_fullscreen_entire_00102, R.drawable.ds_fullscreen_entire_00103,R.drawable.ds_fullscreen_entire_00104};

    

     /**
     * @param imageView 
     * @return progress dialog animation
     */ 
    public FramesSequenceAnimation createProgressAnim(ImageView imageView)
    {  
        return new FramesSequenceAnimation(imageView, mProgressAnimFrames, FPS);
    }

    
    /**
     * AnimationPlayer. Plays animation frames sequence in loop
     */
	public class FramesSequenceAnimation 
	{
	    private int[] mFrames; // animation frames
	    private int mIndex; // current frame
	    private boolean mShouldRun; // true if the animation should continue running. Used to stop the animation
	    private boolean mIsRunning; // true if the animation currently running. prevents starting the animation twice
	    private SoftReference<ImageView> mSoftReferenceImageView; // Used to prevent holding ImageView when it should be dead.
	    private Handler mHandler;
	    private int mDelayMillis;
	 
	    
	    private Bitmap mBitmap = null;
	    private BitmapFactory.Options mBitmapOptions;
	
	    public FramesSequenceAnimation(ImageView imageView, int[] frames, int fps) 
	    {
	        mHandler = new Handler();
	        mFrames = frames;
	        mIndex = -1;
	        mSoftReferenceImageView = new SoftReference<ImageView>(imageView);
	        mShouldRun = false;
	        mIsRunning = false;
	        mDelayMillis = 1000 / fps;
	
	        imageView.setImageResource(mFrames[0]);
	
	        // use in place bitmap to save GC work (when animation images are the same size & type)
	        Bitmap bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
	        int width = bmp.getWidth();
	        int height = bmp.getHeight();
	        Bitmap.Config config = bmp.getConfig();
	        mBitmap = Bitmap.createBitmap(width, height, config);
	        mBitmapOptions = new BitmapFactory.Options();
	       
	        // setup bitmap reuse options. 
	        mBitmapOptions.inBitmap = mBitmap;
	        mBitmapOptions.inMutable = true;
	        mBitmapOptions.inSampleSize = 4; //Dave: Changed image size to stop out of memory error.
	        mBitmapOptions.inScaled = true;
	        
	        
	        
	    }
	    
	    private int getNext() 
	    {
	        mIndex++;
	        if (mIndex >= mFrames.length)
	            mIndex = 0;
	        return mFrames[mIndex];
	    }
	
	    Integer count = 0;
	    
	    /**
	     * Starts the animation
	     */
	    public synchronized void start() 
	    {
	        mShouldRun = true;
	        if (mIsRunning)
	            return;
	
	        Runnable runnable = new Runnable() 
	        {
	            @Override 
	            public void run() 
	            {
	                ImageView imageView = mSoftReferenceImageView.get();
	                if (!mShouldRun || imageView == null || count == mFrames.length-1) 
	                {
	                    mIsRunning = false;
//	                    if (mOnAnimationStoppedListener != null) 
//	                    {
//	                    	mOnAnimationStoppedListener.AnimationStopped();
//	                    }
	                    onAnimChange.onFinish();
	                    return;
	                }
	
	                mIsRunning = true;
	                mHandler.postDelayed(this, mDelayMillis);
	
	                if (imageView.isShown()) 
	                {
	                    int imageRes = getNext();
	                    if (mBitmap != null) 	                    
	                    {
	                        Bitmap bitmap = null;
	                        try 
	                        {
	                            bitmap = BitmapFactory.decodeResource(imageView.getResources(), imageRes, mBitmapOptions);
	                        } 
	                        catch (Exception e) 
	                        {
	                            e.printStackTrace();
	                        }
	                        if (bitmap != null) 
	                        {
	                            imageView.setImageBitmap(bitmap);
	                            imageView.bringToFront();
	                            imageView.setFocusableInTouchMode(true);
	                            count++;
	                        } 
	                        else 
	                        {
	                            imageView.setImageResource(imageRes);
	                            mBitmap.recycle();
	                            mBitmap = null;
	                        }
	                    } 
	                    else 
	                    {
	                        imageView.setImageResource(imageRes);
	                        count++;
	                    }
	                }
	
	            }
	        };
	
	        mHandler.post(runnable);
	    }
	
	        /**
	         * Stops the animation
	         */
	        public synchronized void stop() 
	        {
	            mShouldRun = false;
	        }
    }
}
