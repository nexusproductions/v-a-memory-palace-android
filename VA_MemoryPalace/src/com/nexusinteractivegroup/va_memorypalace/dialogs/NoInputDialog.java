package com.nexusinteractivegroup.va_memorypalace.dialogs;

import com.nexusinteractivegroup.va_memorypalace.R;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class NoInputDialog extends Activity
{
	AsyncTask noInputTask; 
	
	private Integer option = 0;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
	
		if (savedInstanceState == null)
		{
			try
			{
				Bundle args = getIntent().getBundleExtra("args");
				option = args.getInt("option");
			}
			catch (Exception e)
			{
				option = 0;
			}
			
		}
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		setContentView (R.layout.no_input);
		
		ImageButton btnCancel = (ImageButton) findViewById(R.id.btnCancel);
		ImageButton btnAccept;

		noInputTask  = new AsyncTask()
		{

			@Override
			protected Object doInBackground(Object... arg0)
			{
				try
				{					
						synchronized(this)
						{
							wait(60000); // 60 secs							
						}								
						
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();	// need to remove this
				}
				
				return null;
			}

			@Override
			protected void onPostExecute(Object result)
			{
			
				NoInputDialog.this.setResult(99);				
				NoInputDialog.this.finish();
				super.onPostExecute(result);
			}			
			
		}.execute(null,null,null);

		
		btnAccept= (ImageButton) findViewById(R.id.btnAccept);
		
		((RelativeLayout) findViewById(R.id.testConfirm)).setBackgroundResource(R.drawable.no_input_box);
			

		btnAccept.setOnClickListener(saveListener);
		btnCancel.setOnClickListener(cancelListener);
				
			
	}

	Button.OnClickListener cancelListener = new Button.OnClickListener()
	{

		@Override
		public void onClick(View v)
		{	
			// use the default dialog cancel method which can 
			// trigger the OnCancel listener if we need i					

			Intent data = new Intent();
			data.putExtra("option", 5);
			NoInputDialog.this.setResult(5, data);
			
			finish();
		}
		
	};
	
	Button.OnClickListener saveListener = new Button.OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			NoInputDialog.this.setResult(99);
			finish();		
		}
		
	};	



}
