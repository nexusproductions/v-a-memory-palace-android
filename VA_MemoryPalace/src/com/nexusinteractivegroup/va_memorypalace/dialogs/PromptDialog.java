package com.nexusinteractivegroup.va_memorypalace.dialogs;

import com.nexusinteractivegroup.va_memorypalace.DrawActivity;
import com.nexusinteractivegroup.va_memorypalace.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class PromptDialog extends Activity 
{
	
	//AsyncTask noInputTask; 
	
	private Integer option = 0;
	private static PromptDialog staticPrompt;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
	
		if (savedInstanceState == null)
		{
			Bundle args = getIntent().getBundleExtra("args");
			option = args.getInt("option");
		}
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setContentView (R.layout.home_confirm);
		
		staticPrompt = PromptDialog.this;
		
		ImageButton btnCancel = (ImageButton) findViewById(R.id.btnCancel);
		ImageButton btnAccept;

		
		ImageButton btnSaveCancel;
		ImageButton btnSave;
		
		
		btnAccept= (ImageButton) findViewById(R.id.btnAccept);
		
		switch (option)
		{
			//	home
			case 1:				
				((RelativeLayout) findViewById(R.id.testConfirm)).setBackgroundResource(R.drawable.home_box);				

				btnAccept.setOnClickListener(saveListener);
				btnCancel.setOnClickListener(cancelListener);
				
				break;
			// save
			case 2:
				setContentView (R.layout.save_dialog);
				
				btnSaveCancel = (ImageButton) findViewById(R.id.btnDlgCancel);
				btnSave= (ImageButton) findViewById(R.id.btnDlgSave);
				
				//((RelativeLayout) v.findViewById(R.id.testConfirm)).setBackgroundResource(R.drawable.mp_confirmation_box);

				btnSave.setOnClickListener(saveListener);
				btnSaveCancel.setOnClickListener(cancelListener);			
				
				break;
				
			// reset
			case 3:
				((RelativeLayout) findViewById(R.id.testConfirm)).setBackgroundResource(R.drawable.reset_box);
			

				btnAccept.setOnClickListener(saveListener);
				btnCancel.setOnClickListener(cancelListener);
				
				break;
				
			default :			
				// = (View) inflater.inflate(R.layout.confirmation_test, container);
				
				((RelativeLayout) findViewById(R.id.testConfirm)).setBackgroundResource(R.drawable.no_input_box);
				

				btnAccept.setOnClickListener(saveListener);
				btnCancel.setOnClickListener(cancelListener);
				break;
		}
		
	}

	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		this.setResult(99);
		finish();
		super.onActivityResult(requestCode, resultCode, data);
	}



	Button.OnClickListener cancelListener = new Button.OnClickListener()
	{

		@Override
		public void onClick(View v)
		{	
			// use the default dialog cancel method which can 
			// trigger the OnCancel listener if we need i					

			Intent data = new Intent();
			data.putExtra("option", 5);
			PromptDialog.this.setResult(22, data);
			
			finish();
		}
		
	};
	
	Button.OnClickListener saveListener = new Button.OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			Intent data = new Intent();
			data.putExtra("option", option);
			PromptDialog.this.setResult(22, data);
			finish();
//			ActionConfirmation.this.dismiss();
			
		}
		
	};	

}
