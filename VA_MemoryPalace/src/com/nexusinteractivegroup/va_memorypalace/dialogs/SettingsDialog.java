package com.nexusinteractivegroup.va_memorypalace.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.nexusinteractivegroup.va_memorypalace.R;

public class SettingsDialog extends Activity {
	EditText ed;
	EditText ed2;

	public SharedPreferences settingsPrefs;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setContentView(R.layout.settings_dialog);

		if (settingsPrefs == null)
			settingsPrefs = getSharedPreferences(
                    "appSettings", MODE_PRIVATE);

		ed = (EditText) findViewById(R.id.IPText);
		ed2 = (EditText) findViewById(R.id.IPText2);

		if (settingsPrefs != null){
			ed.setText(settingsPrefs.getString("local", getResources().getString(R.string.destIP)));
			ed2.setText(settingsPrefs.getString("remote", getResources().getString(R.string.destIPRemote)));
		}else{
			ed.setText(getResources().getString(R.string.destIP));
			ed2.setText(getResources().getString(R.string.destIPRemote));
		}

		ImageButton btnCancel = (ImageButton) findViewById(R.id.btnCancel);
		ImageButton btnAccept;

		btnAccept = (ImageButton) findViewById(R.id.btnAccept);
		btnAccept.setOnClickListener(saveListener);
		btnCancel.setOnClickListener(cancelListener);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		this.setResult(99);
		finish();
		super.onActivityResult(requestCode, resultCode, data);
	}

	Button.OnClickListener cancelListener = new Button.OnClickListener() {

		@Override
		public void onClick(View v) {
			// use the default dialog cancel method which can
			// trigger the OnCancel listener if we need i

			Intent data = new Intent();
			data.putExtra("option", 5);
			SettingsDialog.this.setResult(122, data);

			finish();
		}

	};

	Button.OnClickListener saveListener = new Button.OnClickListener() {

		@Override
		public void onClick(View v) {

			SharedPreferences.Editor editor = getSharedPreferences(
                    "appSettings", MODE_PRIVATE).edit();
			editor.putString("local", ed.getText().toString());
			editor.putString("remote", ed2.getText().toString());
			editor.commit();
			SettingsDialog.this.setResult(122, null);
			finish();

		}

	};

}
