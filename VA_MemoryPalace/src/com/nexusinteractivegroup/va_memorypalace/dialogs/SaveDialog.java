package com.nexusinteractivegroup.va_memorypalace.dialogs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.boxbe.pub.email.EmailAddress;
import com.nexusinteractivegroup.va_memorypalace.DrawActivity;
import com.nexusinteractivegroup.va_memorypalace.R;
import com.nexusinteractivegroup.va_memorypalace.Startup;
import com.nexusinteractivegroup.va_memorypalace.animation.AnimationsContainer;
import com.nexusinteractivegroup.va_memorypalace.animation.AnimationsContainer.AnimInterface;
import com.nexusinteractivegroup.va_memorypalace.animation.AnimationsContainer.FramesSequenceAnimation;
import com.nexusinteractivegroup.va_memorypalace.util.CustomEditText;
import com.nexusinteractivegroup.va_memorypalace.util.SoftKeyboard;

public class SaveDialog extends Activity {
	Runnable noInputTask;
	Handler noInputHandler;
	static Integer triggerCounter;
	Boolean exited;
	Boolean noInputDialog;
	Boolean started;

	ValueAnimator accelAnimator;
	ImageView endAnimImage;
	ImageView errorMessage;
	Handler animStart;

	StringBuilder sbSVG;

	String filePath;
	File SVGFile;
	String fileDate;

	OutputStream os;

	SimpleDateFormat sf = new SimpleDateFormat("ddMMyy_hhmmss_SS");

	final String END_LINE = "\">\n</path>\n</g>\n<g>\n";
	final String END_FILE = "\">\n</path>\n</g>\n</g>\n</svg>";
	final String END_EMPTY_FILE = "\n</g>\n</svg>";
	final String START_LINE = "<path fill=\"none\" stroke=\"#000000\" stroke-width=\"1\" d=\"M";
	final String HEADER = "<?xml version=\"1.0\" standalone=\"yes\"?>"
			+ "<svg xmlns='http://www.w3.org/2000/svg' width=\"800px\" height=\"1280px\" version=\"1.1\">"
			+ "<g transform=\"translate(0,0)\">\n<g>\n";
	final Integer firstUndoMarker = HEADER.length() - 1;

	private Integer option = 0;

	RadioButton under18Radio, over18Radio;
	CustomEditText emailText;
	private boolean isUnder18 = false;

	Boolean checkedRadios = false;

	String destination;
	String destinationRemote;

	public SharedPreferences settingsPrefs;

	RelativeLayout mainArea, mainArea2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		if (savedInstanceState == null) {
			Bundle args = getIntent().getBundleExtra("args");
			option = args.getInt("option");
		}
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setContentView(R.layout.save_dialog);

		if (settingsPrefs == null)
			settingsPrefs = getSharedPreferences("appSettings",
					MODE_PRIVATE);

		started = true;
		this.setFinishOnTouchOutside(false);
		mainArea = (RelativeLayout) findViewById(R.id.RelativeLayout1);
		mainArea2 = (RelativeLayout) findViewById(R.id.RelativeLayout2);
		emailText = (CustomEditText) findViewById(R.id.emailText);

		activityRootView = findViewById(R.id.RelativeLayout1);

		emailText.setKeyListener(new KeyListener() {

			@Override
			public void clearMetaKeyState(View arg0, Editable arg1, int arg2) {
				triggerCounter = 0;
			}

			@Override
			public int getInputType() {
				triggerCounter = 0;
				return 0;
			}

			@Override
			public boolean onKeyDown(View arg0, Editable arg1, int arg2,
					KeyEvent arg3) {
				triggerCounter = 0;
				return false;
			}

			@Override
			public boolean onKeyOther(View arg0, Editable arg1, KeyEvent arg2) {
				triggerCounter = 0;
				return false;
			}

			@Override
			public boolean onKeyUp(View arg0, Editable arg1, int arg2,
					KeyEvent arg3) {
				triggerCounter = 0;
				return false;
			}

		});

		emailText.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				emailText.post(new Runnable() {
					@Override
					public void run() {
						emailText.onTouchEvent(null);
					}
				});
			}
		});

		TextView tv1 = (TextView) findViewById(R.id.textView1);
		TextView tv2 = (TextView) findViewById(R.id.TextView01);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/interstate_regular.otf");

		triggerCounter = 0;

		exited = false;

		emailText.setTypeface(tf);
		tv1.setTypeface(tf);
		tv2.setTypeface(tf);

		emailText.setInputType(InputType.TYPE_MASK_VARIATION);
		errorMessage = (ImageView) findViewById(R.id.errormessage);

		emailText.setLongClickable(false);
		emailText.setOnLongClickListener(new EditText.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				v.setLongClickable(false);
				v.cancelLongPress();
				v.clearFocus();
				return false;
			}

		});

		ImageButton btnSaveCancel;
		ImageButton btnSave;

		btnSaveCancel = (ImageButton) findViewById(R.id.btnDlgCancel);
		btnSave = (ImageButton) findViewById(R.id.btnDlgSave);

		btnSave.setOnClickListener(saveListener);
		btnSaveCancel.setOnClickListener(cancelListener);

		noInputDialog = false;

		noInputHandler = new Handler();

		noInputTask = new Runnable() {

			@Override
			public void run() {
				if (triggerCounter < 12) {
					triggerCounter++;
				} else {
					if (!noInputDialog) {

						noInputDialog = true;
						triggerCounter = 0;

						exited = true;
						Intent i = new Intent(SaveDialog.this,
								NoInputDialog.class);
						SaveDialog.this.startActivityForResult(i, 212);

						return;
					}
				}
				if (!exited) {
					noInputHandler.postDelayed(noInputTask, 5000);
				}
			}

		};

		noInputHandler.postDelayed(noInputTask, 5000);

		endAnimImage = (ImageView) findViewById(R.id.end_anim_view2);

		animStart = new Handler();

		under18Radio = (RadioButton) findViewById(R.id.radioButton2);
		over18Radio = (RadioButton) findViewById(R.id.radioButton1);

		under18Radio.setOnCheckedChangeListener(ocListener);
		over18Radio.setOnCheckedChangeListener(ocListener);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		return true;
	}

	Boolean stopClick = false;

	View activityRootView;

	private void stopClicking() {

		stopClick = true;
		this.emailText.setEnabled(false);
		emailText.setClickable(false);

		this.endAnimImage.setClickable(false);
		mainArea.setEnabled(false);
		mainArea.setClickable(false);
		mainArea2.setEnabled(false);
		mainArea2.setClickable(false);

		for (int i = 1; i < mainArea2.getChildCount(); i++) {
			mainArea2.getChildAt(i).setClickable(false);
		}

	}

	public void onRadioButtonClicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();
		final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.radioButton2: // Under 18
			if (checked)
				emailText.setText("");
			emailText.setHint("");
			emailText.setEnabled(false);
			imm.hideSoftInputFromWindow(emailText.getWindowToken(), 0);
			isUnder18 = true;
			break;
		case R.id.radioButton1: // Over 18
			if (checked)
				emailText.setEnabled(true);
			emailText.setHint("Enter email address");
			isUnder18 = false;

			imm.showSoftInput(emailText, InputMethodManager.SHOW_FORCED);
			if (emailText.getText().length() == 0) {
				errorMessage.setImageResource(R.drawable.email_box_error1);
			}
			break;
		}
	}

	OnCheckedChangeListener ocListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
			triggerCounter = 0;
			checkedRadios = true;
			// if (under18Radio.isChecked()) {
			// // if (arg0 != over18Radio){ //You have changed from over18 to
			// // under18
			//
			//
			// } else if (over18Radio.isChecked()) {
			//
			// } else {
			// stopClick=false;
			// InputMethodManager imm = (InputMethodManager)
			// getSystemService(Context.INPUT_METHOD_SERVICE);
			// imm.hideSoftInputFromWindow(emailText.getWindowToken(), 0);
			// }

		}
	};

	Button.OnClickListener cancelListener = new Button.OnClickListener() {

		@Override
		public void onClick(View v) {
			// use the default dialog cancel method which can
			// trigger the OnCancel listener if we need i
			exited = true;
			Intent data = new Intent();
			data.putExtra("option", 5);
			SaveDialog.this.setResult(22, data);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(emailText.getWindowToken(), 0);
			finish();
		}

	};

	Button.OnClickListener saveListener = new Button.OnClickListener() {

		@SuppressWarnings("unchecked")
		@Override
		public void onClick(View v) {
			exited = true;
			
			if (isNetworkAvailable()){
			if (settingsPrefs != null){
				settingsPrefs = getSharedPreferences("appSettings",
						MODE_PRIVATE);
			destination = settingsPrefs.getString("local", getResources().getString(R.string.destIP));
			destinationRemote = settingsPrefs.getString("remote", getResources().getString(R.string.destIPRemote));
			}else{
				destination = getResources().getString(R.string.destIP);
				destinationRemote = getResources().getString(R.string.destIPRemote);
			}
			if (emailText.getText().toString().equals("ADMINSETTINGS")) {
				Intent i = new Intent(SaveDialog.this, SettingsDialog.class);
				SaveDialog.this.startActivityForResult(i, 122);
				return;
			} else {

				// check for email address first
				if ((EmailAddress
						.isValidMailbox(emailText.getText().toString()) || isUnder18)
						&& checkedRadios) {
					stopClicking();

					errorMessage.setVisibility(View.INVISIBLE);

					Intent data = new Intent();
					data.putExtra("option", option);
					SaveDialog.this.setResult(22, data);

					sbSVG = DrawActivity.getSbSVG();

					AnimationsContainer anc = AnimationsContainer.getInstance();
					anc.setAnimInterface(animEnded);
					final FramesSequenceAnimation anim = anc
							.createProgressAnim(endAnimImage);

					accelAnimator = ObjectAnimator.ofFloat(endAnimImage, "y",
							1300f, 0f).setDuration(1500);
					accelAnimator
							.setInterpolator(new DecelerateInterpolator(2f));

					new AsyncTask() {

						@Override
						protected Object doInBackground(Object... arg0) {
							createSvg();
							return null;
						}

						@Override
						protected void onPostExecute(Object result) {
							new AsyncTask() {

								@Override
								protected Object doInBackground(
										Object... params) {
									uploadSVG();
									return null;
								}

							}.execute(null, null, null);

							super.onPostExecute(result);
						}

					}.execute(null, null, null);

					if (over18Radio.isChecked()) {
						new AsyncTask() {

							@Override
							protected Object doInBackground(Object... params) {
								uploadSVGExternal();
								return null;
							}

						}.execute(null, null, null);
					}

					animStart.post(new Thread() {
						@Override
						public void run() {

							try {

								InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(
										emailText.getWindowToken(), 0);

								endAnimImage.setVisibility(View.VISIBLE);
								endAnimImage.bringToFront();

								anim.start();

								stopClicking();
								// SaveDialog.this.finish(); //Kill dialog to
								// stop clicking error

								accelAnimator.start();
								accelAnimator
										.addListener(new Animator.AnimatorListener() {

											@Override
											public void onAnimationStart(
													Animator animation) {
												getWindow()
														.setSoftInputMode(
																WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
											}

											@Override
											public void onAnimationRepeat(
													Animator animation) {
												// Not needed
											}

											@Override
											public void onAnimationEnd(
													Animator animation) {
												// Not needed
											}

											@Override
											public void onAnimationCancel(
													Animator animation) {
												// Not needed
											}
										});

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				} else {
					if (!checkedRadios) {
						errorMessage
								.setImageResource(R.drawable.valid_checkbox);
						errorMessage.setVisibility(View.VISIBLE);
					} else {
						errorMessage
								.setImageResource(R.drawable.valid_email_box);
						errorMessage.setVisibility(View.VISIBLE);
					}

				}
			}
			}else{
				Toast.makeText(getBaseContext(), "No Wifi Connection", Toast.LENGTH_LONG).show();
			}
		}
	};

	private void createSvg() {
		if (sbSVG.lastIndexOf("</g>\n<g> ") > -1) {
			String gCheck = sbSVG.substring(sbSVG.lastIndexOf("</g>\n<g> "),
					sbSVG.length());

			if (gCheck.equals("</g>\n<g> ")) {
				sbSVG.delete(sbSVG.lastIndexOf("<g>"), sbSVG.length());
				sbSVG.append(END_EMPTY_FILE);
			} else {
				sbSVG.append(END_FILE);
			}
		} else {
			// sbSVG = sbSVG.delete(sbSVG.lastIndexOf("<g>"), sbSVG.length() -
			// 1);
			sbSVG.append(END_FILE);
		}

		// double check the file

		String error = "<g>\n" + END_LINE;
		Integer startIndex = sbSVG.indexOf(error);

		Integer toIndex;

		while (startIndex > 0) {
			toIndex = startIndex + error.length() - 1;
			sbSVG = sbSVG.replace(startIndex, toIndex, "<g>");
			startIndex = sbSVG.indexOf(error);
		}

		String errorBlank = "<g>\n\">\n</path>\n"; // Undo first and save error
		Integer startIndexBlank = sbSVG.indexOf(errorBlank);
		String errorBlank2 = "<g>\">\n</path>\n"; // Reset and save error
		Integer startIndexBlank2 = sbSVG.indexOf(errorBlank2);

		Integer toIndexEnd;
		Integer indexStart = 0;
		if (startIndexBlank > 0 || startIndexBlank2 > 0) {
			if (startIndexBlank > 0) {
				indexStart = startIndexBlank;
			} else {
				indexStart = startIndexBlank2;
			}
		}

		while (indexStart > 0) {
			toIndexEnd = indexStart + error.length();
			sbSVG = sbSVG.replace(indexStart, toIndexEnd, "<g>"); // "</g>"
			indexStart = sbSVG.indexOf(error);
		}

		String errorG = "<g><g>";
		Integer startIndexG = sbSVG.indexOf(errorG);

		if (startIndexG > 0) {
			toIndex = startIndexG + errorG.length() - 1;
			sbSVG = sbSVG.replace(startIndexG, toIndex, "<g>");
		}

		String errorNoShev = "<g>path fill";
		Integer startIndexNoShev = sbSVG.indexOf(errorNoShev);

		if (startIndexNoShev > 0) {
			toIndex = startIndexNoShev + errorNoShev.length() - 1;
			sbSVG = sbSVG.replace(startIndexNoShev, toIndex, "<g><path fill");
		}

		String errorShev = "<g>>";
		Integer startIndexShev = sbSVG.indexOf(errorShev);

		if (startIndexShev > 0) {
			toIndex = startIndexShev + errorShev.length() - 1;
			sbSVG = sbSVG.replace(startIndexShev, toIndex, "<g>");
		}

		String errorDoublePath = "<path fill=\"none\" s <g><path fill";
		Integer startIndexDoublePath = sbSVG.indexOf(errorDoublePath);

		if (startIndexDoublePath > 0) {
			toIndex = startIndexDoublePath + errorDoublePath.length() - 1;
			sbSVG = sbSVG.replace(startIndexDoublePath, toIndex, "<path fill");
		}
		
		try {

			fileDate = sf.format(Calendar.getInstance().getTime());

			filePath = Environment.getExternalStorageDirectory() + "/Download/"
					+ fileDate + "_tablet.svg";

			SVGFile = new File(filePath);
			startIndex = sbSVG.indexOf("<g>\n\">");
			while (startIndex > 0) {
				toIndex = startIndex + error.length() - 1;
				sbSVG = sbSVG.replace(startIndex, toIndex, "<g>");
				startIndex = sbSVG.indexOf("<g>\n\">");

			}
			finalst = sbSVG.toString().replace(error, "<g>");

			os = new FileOutputStream(SVGFile, true);
			os.write(finalst.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	String finalst;

	private void uploadSVG() {

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httpPost = new HttpPost(destination);

		try {
			Log.i("UPLOAD_SVG", "Sending SVG");
			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			BasicNameValuePair vp = (new BasicNameValuePair("image", filePath));

			entity.addPart(vp.getName(), new FileBody(new File(vp.getValue())));

			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost, localContext);

			// int counter = 0;

			Log.i("UPLOAD_SVG", "SVG SENT");

			if (response.getStatusLine().getStatusCode() != 200) {
				response = httpClient.execute(httpPost, localContext);
				// counter ++;
			} else {
				Log.i("UPLOAD_SVG", "RESPONSE = "
						+ response.getStatusLine().getStatusCode());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void uploadSVGExternal() {

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httpPost = new HttpPost(destinationRemote);

		try {
			// MultipartEntity entity = new
			// MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			//
			//
			//
			// BasicNameValuePair vp = (new
			// BasicNameValuePair("memory",filePath));
			//
			// entity.addPart(vp.getName(), new FileBody(new File
			// (vp.getValue())));

			JSONObject jb = new JSONObject();
			JSONObject jbData = new JSONObject();

			try {
				jbData.put("email", emailText.getText().toString());
				if (finalst == null) {
					finalst = "test";
				}

				jbData.put("svg", finalst);
			} catch (JSONException e) {

				e.printStackTrace();
			}

			try {
				jb.put("data", jbData);
			} catch (JSONException e) {

				e.printStackTrace();
			}

			// HttpParams jsonParam = new BasicHttpParams();
			//
			// jsonParam.setParameter("email",jb );
			// jsonParam.setParameter("svg",jb2 );

			httpPost.setEntity(new ByteArrayEntity(jb.toString().getBytes(
					"UTF8")));

			httpPost.setHeader("Content-type",
					"multipart/related; type=\"application/json\"");

			// httpPost.setEntity(entity);

			Log.i("UPLOAD_SVG_REMOTE", "SENDING ...");

			HttpResponse response = httpClient.execute(httpPost, localContext);

			// int counter = 0;
			Log.i("UPLOAD_SVG_REMOTE", "SENT");
			if (response.getStatusLine().getStatusCode() != 200) {
				response = httpClient.execute(httpPost, localContext);
				// counter ++;
			}
			Log.i("UPLOAD_SVG_REMOTE", "RESPONSE "
					+ response.getStatusLine().toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void onKeyPress() {
		triggerCounter = 0;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		SoftKeyboard.notRestarting();

		switch (resultCode) {
		case 5: {
			triggerCounter = 0;
			exited = false;
			noInputDialog = false;
			emailText.requestFocus();

			noInputHandler.postDelayed(noInputTask, 5000);
			started = false;
			break;
		}

		case 6: //
			this.setResult(99);
			finish();
			break;
		case 99: {
			this.setResult(99);
			finish();
			break;
		}
		case 122: {
			if (settingsPrefs == null)
				settingsPrefs = getSharedPreferences("appSettings",
						MODE_PRIVATE);
			destination = settingsPrefs.getString("local", getResources().getString(R.string.destIP));
			destinationRemote = settingsPrefs.getString("remote", getResources().getString(R.string.destIPRemote));
			emailText.setText("");
			triggerCounter = 0;
			noInputDialog = false;
			exited = false;
			noInputHandler.postDelayed(noInputTask, 5000);
			emailText.requestFocus();

			break;
		}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		super.onResume();
	}

	AnimationsContainer.AnimInterface animEnded = new AnimInterface() {

		@Override
		public void onFinish() {
			Intent endIntent = new Intent(SaveDialog.this, Startup.class);
			endIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			Startup.returning = true;

			startActivity(endIntent);
			finish();
		}
	};

	public boolean isNetworkAvailable() {
	    ConnectivityManager cm = (ConnectivityManager) 
	      getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
	    // if no network is available networkInfo will be null
	    // otherwise check if we are connected
	    if (networkInfo != null && networkInfo.isConnected()) {
	        return true;
	    }
	    return false;
	} 
}
