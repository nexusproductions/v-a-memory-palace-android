/*
 * Copyright (C) 2008-2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nexusinteractivegroup.va_memorypalace.util;

import java.util.HashMap;
import java.util.Map;

import com.nexusinteractivegroup.va_memorypalace.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.PopupWindow;

public class CustomKeyboardView extends KeyboardView 
{
	

    @Override
	public void onDraw(Canvas canvas)
	{
		// TODO Auto-generated method stub
		super.onDraw(canvas);
	}

	@Override
	public void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	public void setPopupOffset(int x, int y)
	{
		// TODO Auto-generated method stub
		super.setPopupOffset(x, y);
	}

	@Override
	public void setProximityCorrectionEnabled(boolean enabled)
	{
		// TODO Auto-generated method stub
		super.setProximityCorrectionEnabled(enabled);
	}

	static final int KEYCODE_OPTIONS = -100;

    public CustomKeyboardView(Context context, AttributeSet attrs) 
    {
        super(context, attrs);

      
    }

    public CustomKeyboardView(Context context, AttributeSet attrs, int defStyle) 
    {
    	
        super(context, attrs, defStyle);
        
    }

   	@Override
	public boolean onTouchEvent(MotionEvent me)
	{
		return super.onTouchEvent(me);
	}

	
 
	@Override
    protected boolean onLongPress(Key popupKey) 
    {
		
		
        if (popupKey.codes[0] == Keyboard.KEYCODE_CANCEL) 
        {
            getOnKeyboardActionListener().onKey(KEYCODE_OPTIONS, null);
            return true;
        } 
        else 
        {
            return super.onLongPress(popupKey);
        }
    }

	@Override
	public void setOnKeyboardActionListener(OnKeyboardActionListener listener)
	{
		super.setOnKeyboardActionListener(listener);
	}
	
	@Override
	public void onClick(View v) 
 	{
        super.onClick(v);
  	}
	

}
