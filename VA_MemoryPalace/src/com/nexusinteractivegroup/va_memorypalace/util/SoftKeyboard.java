/*
 * Copyright (C) 2008-2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nexusinteractivegroup.va_memorypalace.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.text.InputType;
import android.text.method.MetaKeyKeyListener;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;

import java.util.ArrayList;
import java.util.List;

import com.nexusinteractivegroup.va_memorypalace.R;
import com.nexusinteractivegroup.va_memorypalace.dialogs.SaveDialog;


public class SoftKeyboard extends InputMethodService implements KeyboardView.OnKeyboardActionListener 
{	
    
    
    /**
     * This boolean indicates the optional example code for performing
     * processing of hard keys in addition to regular text generation
     * from on-screen interaction.  It would be used for input methods that
     * perform language translations (such as converting text entered on 
     * a QWERTY keyboard to Chinese), but may not be used for input methods
     * that are primarily intended to be used for on-screen text entry.
     */
    

    private InputMethodManager mInputMethodManager;

    private CustomKeyboardView mInputView;
    
    private StringBuilder mComposing = new StringBuilder();
    private int mLastDisplayWidth;
    private boolean mCapsLock;
    private long mLastShiftTime;
    private long mMetaState;
    
   
    
    private CustomKeyboard mQwertyKeyboard;
    
    private CustomKeyboard mCurKeyboard;
    
    private String mWordSeparators;
    


    @Override public void onCreate() 
    {
        super.onCreate();
        mInputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        
        mWordSeparators = getResources().getString(R.string.word_separators);
        
    }
    
    /**
     * This is the point where you can do all of your UI initialization.  It
     * is called after creation and any configuration change.
     */
    @Override public void onInitializeInterface() 
    {
        if (mQwertyKeyboard != null) 
        {
            // Configuration changes can happen after the keyboard gets recreated,
            // so we need to be able to re-build the keyboards if the available
            // space has changed.
    
        	int displayWidth = getMaxWidth();
            if (displayWidth == mLastDisplayWidth) return;
            mLastDisplayWidth = displayWidth;
        }
        mQwertyKeyboard = new CustomKeyboard(this, R.xml.qwerty);
        
    
    }
    
    /**
     * Called by the framework when your view for creating input needs to
     * be generated.  This will be called the first time your input method
     * is displayed, and every time it needs to be re-created such as due to
     * a configuration change.
     */
    @Override public View onCreateInputView() 
    {
        mInputView = (CustomKeyboardView) getLayoutInflater().inflate(
                R.layout.input, null);
        mInputView.setOnKeyboardActionListener(this);
        mInputView.setKeyboard(mQwertyKeyboard);
        mInputView.setShifted(true);
        
        return mInputView;
    }

    /**
     * Called by the framework when your view for showing candidates needs to
     * be generated, like {@link #onCreateInputView}.
     */
    @Override public View onCreateCandidatesView() 
    {
        return null;
    }

    /**
     * This is the main point where we do our initialization of the input method
     * to begin operating on an application.  At this point we have been
     * bound to the client, and are now receiving all of the detailed information
     * about the target of our edits.
     */
    @Override public void onStartInput(EditorInfo attribute, boolean restarting) 
    {
        super.onStartInput(attribute, restarting);
        
        // Reset our state.  We want to do this even if restarting, because
        // the underlying state of the text editor could have changed in any way.
        mComposing.setLength(0);
      
        if (!restarting) 
        {
            // Clear shift states.
            mMetaState = 0;
        }
        
    
        mCurKeyboard = mQwertyKeyboard;
        
       
    }

    /**
     * This is called when the user is done editing a field.  We can use
     * this to reset our state.
     */
    @Override public void onFinishInput() 
    {
      
        
        // Clear current composing text and candidates.
//        mComposing.setLength(0);
//        
//        mCurKeyboard = mQwertyKeyboard;
//        if (mInputView != null) 
//        {
//        	mInputView.setY(0f);            
//        	mInputView.animate().y(250F)
//            .setDuration(1800)
//            .setListener(new AnimatorListenerAdapter() 
//            {
//                @Override
//                public void onAnimationEnd(Animator animation) 
//                {
//                	mInputView.setVisibility(View.GONE);
//                	
//                }
//            });
            //mInputView.closing();
//        	mInputView.animate().start();        	
//        }
        super.onFinishInput();
    }
    
  
    static Boolean notshowKeyboard = false;
    
    public static void notRestarting()
    {
    	notshowKeyboard = true;
    }
    
    @Override 
    public void onStartInputView(EditorInfo attribute, boolean restarting) 
    {
    	//if (notshowKeyboard)
    	//{
	    	super.onStartInputView(attribute, restarting);
	        
	    	// Apply the selected keyboard to the input view.
	        mInputView.setY(250f);
	        mInputView.setAlpha(0f);
	    	mInputView.setKeyboard(mCurKeyboard);
	    	
	    	if (mInputView.getVisibility() == View.GONE)
	    		mInputView.setVisibility(View.VISIBLE); 
	    	
	        mInputView.animate().y(0f).alpha(255f)
	        .setDuration(500)
	        .setListener(new AnimatorListenerAdapter() 
	        {
	            @Override
	            public void onAnimationEnd(Animator animation) 
	            {
	            //  mInputView.setVisibility(View.GONE);
	            	
	            	
	            	
	            //  mInputView.closing();
	            
	            }
	        });
	        
	        mInputView.animate().start();
//    	}
//    	else
//    	{
//    		onFinishInput();
//    		return;
//    	}
    }

    
    /**
     * Deal with the editor reporting movement of its cursor.
     */
    @Override public void onUpdateSelection(int oldSelStart, int oldSelEnd,
            int newSelStart, int newSelEnd,
            int candidatesStart, int candidatesEnd) {
        super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd,
                candidatesStart, candidatesEnd);
        
        // If the current selection in the text view changes, we should
        // clear whatever candidate text we have.
        if (mComposing.length() > 0 && (newSelStart != candidatesEnd
                || newSelEnd != candidatesEnd)) {
            mComposing.setLength(0);
    
            InputConnection ic = getCurrentInputConnection();
            if (ic != null) {
                ic.finishComposingText();
            }
        }
    }

 
    /**
     * Use this to monitor key events being delivered to the application.
     * We get first crack at them, and can either resume them or let them
     * continue to the app.
     */
    @Override public boolean onKeyDown(int keyCode, KeyEvent event) 
    {


    	switch (keyCode)
        {
            case KeyEvent.KEYCODE_BACK:
                // The InputMethodService already takes care of the back
                // key for us, to dismiss the input method if it is shown.
                // However, our keyboard could be showing a pop-up window
                // that back should dismiss, so we first allow it to do that.
                if (event.getRepeatCount() == 0 && mInputView != null) 
                {
                	if (mInputView.handleBack()) 
                    {
                        return true;
                    }
                }
                break;
                
            case KeyEvent.KEYCODE_DEL:
                // Special handling of the delete key: if we currently are
                // composing text for the user, we want to modify that instead
                // of let the application to the delete itself.
                if (mComposing.length() > 0) 
                {
                    onKey(Keyboard.KEYCODE_DELETE, null);
                    return true;
                }
                break;
                
            case KeyEvent.KEYCODE_ENTER:
                // Let the underlying text editor always handle these.
                return false;
                
            default:
                // For all other keys, if we want to do transformations on
                // text being entered with a hard keyboard, we need to process
                // it and do the appropriate action.
            	   return true;
        }
        
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Use this to monitor key events being delivered to the application.
     * We get first crack at them, and can either resume them or let them
     * continue to the app.
     */
    @Override public boolean onKeyUp(int keyCode, KeyEvent event) 
    {
    	
        return super.onKeyUp(keyCode, event);
    }

    
    /**
     * We want shift on all the time 
     * editor state.
     */
    private void updateShiftKeyState(EditorInfo attr) 
    {
        if (attr != null 
                && mInputView != null && mQwertyKeyboard == mInputView.getKeyboard()) 
        {
            
        	
            
            mInputView.setShifted(true);
        }
    }
    
    /**
     * Helper to determine if a given character code is alphabetic.
     */
    private boolean isAlphabet(int code) {
        if (Character.isLetter(code)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Helper to send a key down / key up pair to the current editor.
     */
    private void keyDownUp(int keyEventCode) {
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));
    }
    
    /**
     * Helper to send a character to the editor as raw key events.
     */
    private void sendKey(int keyCode) 
    {

    	switch (keyCode) 
    	{
            case '\n':
                keyDownUp(KeyEvent.KEYCODE_ENTER);
                break;
            default:
                if (keyCode >= '0' && keyCode <= '9') 
                {
                    keyDownUp(keyCode - '0' + KeyEvent.KEYCODE_0);
                } 
                else 
                {
                    getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
                }
                break;
        }
    }

    // Implementation of KeyboardViewListener

    public void onKey(int primaryCode, int[] keyCodes) 
    {
    	// we can afford this un-checked luxury since there is really only one edit box in the entire application that we need to call this for
       	SaveDialog.onKeyPress();

        if (isWordSeparator(primaryCode)) 
        {
            sendKey(primaryCode);
            updateShiftKeyState(getCurrentInputEditorInfo());
        } 
        else if (primaryCode == Keyboard.KEYCODE_DELETE) 
        {
            handleBackspace();
        } 
        else if (primaryCode == Keyboard.KEYCODE_SHIFT) 
        {
            handleShift();
        } 
        else if (primaryCode == Keyboard.KEYCODE_CANCEL) 
        {
            handleClose();
            return;
        } 
        else if (primaryCode == CustomKeyboardView.KEYCODE_OPTIONS) 
        {
            // Show somethimg
        } else if (primaryCode == Keyboard.KEYCODE_MODE_CHANGE
                && mInputView != null) {
            
        	// do nothing - there is only one mode
        	
        } else {
            handleCharacter(primaryCode, keyCodes);
        }
    }

   
    
    private void handleBackspace() 
    {
        final int length = mComposing.length();
        if (length > 1) {
            mComposing.delete(length - 1, length);
            getCurrentInputConnection().setComposingText(mComposing, 1);
           
        } else if (length > 0) {
            mComposing.setLength(0);
            getCurrentInputConnection().commitText("", 0);
           
        } else {
            keyDownUp(KeyEvent.KEYCODE_DEL);
        }
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    private void handleShift() {
        if (mInputView == null) {
            return;
        }
        
        mInputView.setShifted(true);
    }
    
    private void handleCharacter(int primaryCode, int[] keyCodes) {
        if (isInputViewShown()) 
        {
            if (mInputView.isShifted()) 
            {
                primaryCode = Character.toUpperCase(primaryCode);
            }
        }
        if (isAlphabet(primaryCode) ) 
        {
        	// this is just a technique which can be used to create autotext and auto checking
            mComposing.append((char) primaryCode);
            getCurrentInputConnection().finishComposingText();
            getCurrentInputConnection().commitText(mComposing, 1);
            updateShiftKeyState(getCurrentInputEditorInfo());
         
        } else {
            getCurrentInputConnection().commitText(
                    String.valueOf((char) primaryCode), 1);
        }
    }

    private void handleClose() {

        requestHideSelf(0);
        mInputView.closing();
    }

    private void checkToggleCapsLock() {
        long now = System.currentTimeMillis();
        if (mLastShiftTime + 800 > now) {
            mCapsLock = !mCapsLock;
            mLastShiftTime = 0;
        } else {
            mLastShiftTime = now;
        }
    }
    
    private String getWordSeparators() {
        return mWordSeparators;
    }
    
    public boolean isWordSeparator(int code) {
        String separators = getWordSeparators();
        return separators.contains(String.valueOf((char)code));
    }

    public void swipeRight() 
    {
       // nothing
    }
    
    public void swipeLeft() {
        handleBackspace();
    }

    public void swipeDown() {
        handleClose();
    }

    public void swipeUp() {
    }
    
    public void onPress(int primaryCode) 
    {
    	
    }
    
    public void onRelease(int primaryCode) {
    }

	@Override
	public void onText(CharSequence arg0)
	{
		// TODO Auto-generated method stub

//	    public void onText(CharSequence text) {
//	        InputConnection ic = getCurrentInputConnection();
//	        if (ic == null) return;
//	        ic.beginBatchEdit();
//	        if (mComposing.length() > 0) {
//	            commitTyped(ic);
//	        }
//	        
//	        ic.commitText(text, 0);
//	        ic.endBatchEdit();
//	        updateShiftKeyState(getCurrentInputEditorInfo());
//	    }
//
		
	}
}
