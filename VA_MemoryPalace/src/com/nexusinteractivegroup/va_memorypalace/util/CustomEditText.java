package com.nexusinteractivegroup.va_memorypalace.util;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class CustomEditText extends EditText
{

	
	public CustomEditText(Context context)
	{
		super(context);
		this.setLongClickable(false);
		this.setClickable(true);
	}

	public CustomEditText(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.setLongClickable(false);
		this.setClickable(true);
	}
	public CustomEditText(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		this.setLongClickable(false);
		this.setClickable(true);
	}

	@Override
	public boolean performClick()
	{
		return false;
	}

	// dastardly Samsung !!
	 @Override
	 public boolean onTouchEvent(MotionEvent event) 
	 {
		if (this.isEnabled()){
		 final InputMethodManager imm = (InputMethodManager) this.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
         viewClicked(imm);
       
         imm.showSoftInput(this, InputMethodManager.SHOW_FORCED);
		}else{
			 final InputMethodManager imm = (InputMethodManager) this.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(
						this.getWindowToken(), 0);
		}
         
		 return true;
	 }

	protected void viewClicked(InputMethodManager imm) 
	{
	        if (imm != null) 
	        {
	            imm.viewClicked(this);
	        }
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	@Override
	protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect)
	{
		
		super.onFocusChanged(true, direction, previouslyFocusedRect);
	}
	
	 
	
	
}
