package com.nexusinteractivegroup.va_memorypalace;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.LruCache;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.nexusinteractivegroup.va_memorypalace.animation.AnimationsContainer;
import com.nexusinteractivegroup.va_memorypalace.animation.AnimationsContainer.AnimInterface;
import com.nexusinteractivegroup.va_memorypalace.animation.AnimationsContainer.FramesSequenceAnimation;
import com.nexusinteractivegroup.va_memorypalace.animation.CustomAnimationDrawable;

import com.nexusinteractivegroup.va_memorypalace.dialogs.NoInputDialog;
import com.nexusinteractivegroup.va_memorypalace.dialogs.PromptDialog;
import com.nexusinteractivegroup.va_memorypalace.dialogs.SaveDialog;
import com.nexusinteractivegroup.va_memorypalace.util.SoftKeyboard;

public class DrawActivity extends Activity {

	DrawView dv;
	TextView tv;
	ScrollView sv;
	OutputStream os;

	static StringBuilder sbSVG = new StringBuilder();

	public static Boolean exited;
	public static Boolean noInputDialog;
	public static Integer triggerCounter;

	String filePath;
	File SVGFile;
	String fileDate;

	Handler animStart;
	Handler noInputHandler;

	Boolean drawing = false;

	ImageButton btnHome;
	ImageButton btnReset;
	ImageButton btnSave;
	ImageButton btnUndo;

	ImageView startAnimImage;
	ImageView endAnimImage;

	ArrayList<Integer> undoPointers;

	SimpleDateFormat sf = new SimpleDateFormat("ddMMyy_hhmmss_SS");
	PendingIntent restartIntent;

	CustomAnimationDrawable startAnimation;

	ValueAnimator accelAnimator;

	final String END_LINE = "\">\n</path>\n</g>\n<g>\n";

	final String START_LINE = "<path fill=\"none\" stroke=\"#000000\" stroke-width=\"1\" d=\"M";
	final String HEADER = "<?xml version=\"1.0\" standalone=\"yes\"?>"
			+ "<svg xmlns='http://www.w3.org/2000/svg' width=\"800px\" height=\"1280px\" version=\"1.1\">"
			+ "<g transform=\"translate(0,0)\">\n<g>\n";
	final Integer firstUndoMarker = HEADER.length() - 1;

	Runnable noInputTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.drawing_view);
		
		triggerCounter = 0;
		exited = false;
		noInputDialog = false;

		btnHome = (ImageButton) findViewById(R.id.btnHome);
		btnReset = (ImageButton) findViewById(R.id.btnReset);
		btnSave = (ImageButton) findViewById(R.id.btnSave);
		btnUndo = (ImageButton) findViewById(R.id.btnUndo);

		btnReset.setOnClickListener(onResetClick);
		btnHome.setOnClickListener(onHomeClick);
		btnSave.setOnClickListener(onSaveClick);
		btnUndo.setOnClickListener(onUndoClick);

		dv = (DrawView) findViewById(R.id.draw_view1);
		dv.setSVGInterface(svgout);

		sbSVG = new StringBuilder();

		sbSVG.append(HEADER);

		undoPointers = new ArrayList<Integer>();
		undoPointers.add(firstUndoMarker);

		animStart = new Handler();

		startAnimImage = (ImageView) findViewById(R.id.anim_view);
		endAnimImage = (ImageView) findViewById(R.id.end_anim_view);

		startAnimation = new CustomAnimationDrawable(
				(AnimationDrawable) getResources().getDrawable(
						R.anim.start_animation)) {
			@Override
			public void onAnimationFinish() {
				dv.setEnabled(true);
				dv.animInterim = true;
				return;
			}
		};

		restartIntent = PendingIntent.getActivity(getApplication()
				.getBaseContext(), 0, new Intent(getIntent()), getIntent()
				.getFlags());

		startAnimImage.setImageDrawable(startAnimation);

		animStart.postDelayed(new Thread() {
			@Override
			public void run() {
				if (!drawing) {
					dv.setEnabled(false);
					enableToolBar(false);
					startAnimImage.setVisibility(View.VISIBLE);
					startAnimation.start();
				}
			}
		}, 3000);

		noInputHandler = new Handler();

		noInputTask = new Runnable() {

			@Override
			public void run() {
				if (triggerCounter < 12) {
					triggerCounter++;
				} else {
					if (!noInputDialog && !exited) {

						noInputDialog = true;
						triggerCounter = 0;

						clicked = true;
						Bundle args = new Bundle();
						args.putInt("option", 4);
						Intent i = new Intent(DrawActivity.this,
								NoInputDialog.class);
						i.putExtra("args", args);
						DrawActivity.this.startActivityForResult(i, 22);

						return;
					}
				}
				if (!exited && !noInputDialog) {
					noInputHandler.postDelayed(noInputTask, 5000);
				}
			}

		};

		noInputHandler.postDelayed(noInputTask, 5000);

		// ensure the app can't be crashed out

		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {

			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
				mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 200,
						restartIntent);
				System.exit(2);
			}

		});

	}

	static boolean penOn = false;
	Context ctx;

	private void enableToolBar(Boolean enabled) {
		btnReset.setEnabled(enabled);
		btnHome.setEnabled(enabled);
		btnSave.setEnabled(enabled);
		btnUndo.setEnabled(enabled);

	}

	static Boolean started = false;

	static String lastPoint;
	/*
	 * Implementation of the SVGInterface
	 */
	DrawView.SVGInterface svgout = new DrawView.SVGInterface() {

		@Override
		public void onPath(String point) {
			// Generate SVG string;
			if (point.length() > 0) {
				// record the last length before adding the string
				sbSVG.append(point);
				triggerCounter = 0;
			}
			started = true;
			drawing = true;
			triggerCounter = 0;
		}

		@Override
		public void onMove(String point, Boolean afterAnim) {

			btnUndo.setEnabled(true);
			btnReset.setEnabled(true);
			btnSave.setEnabled(true);
			if (started) {
				sbSVG.append(END_LINE);
				started = false;

			}
			drawing = true;
			triggerCounter = 0;
			if (!afterAnim) {

				Integer lastItem = undoPointers.get(undoPointers.size() - 1);
				Integer lastIndex = sbSVG.length() - 1;

				// can't add (start) pointer twice
				if (lastItem < lastIndex)
					undoPointers.add(lastIndex);

				if (!started) {
					String gCheck = sbSVG.substring(sbSVG.lastIndexOf("<g>"),
							sbSVG.length() - 1);

					if (!gCheck.equals("<g>")) {
						sbSVG.append("<g>");
					}

					sbSVG.append(START_LINE);
					started = true;
				}

			}
			if (!point.contains("null")) {
				sbSVG.append(point);
				lastPoint = point;
			} else
				sbSVG.append(lastPoint);

			if (startAnimImage.isShown()) {
				startAnimation = new CustomAnimationDrawable(
						(AnimationDrawable) getResources().getDrawable(
								R.anim.start_animation_out)) {
					@Override
					public void onAnimationFinish() {
						dv.setEnabled(true);
						enableToolBar(true);
						dv.animInterim = false;
						this.stop();
						triggerCounter = 0;
						startAnimImage.setVisibility(View.GONE);
						return;
					}

					@Override
					public void start() {
						dv.setEnabled(false);
						super.start();
					}

				};

				startAnimImage.setImageDrawable(startAnimation);

				startAnimation.start();
			}
		}

		@Override
		public void onReplace(Integer length) {
			try {
				if (undoPointers.size() > 1) {

					Integer toIndex = sbSVG.length() - length - 1;

					sbSVG = sbSVG.replace(toIndex,
							undoPointers.get(undoPointers.size() - 1) + 1, "");

					// undoPointers.remove(undoPointers.size()-1);

					String error = "<g>\n" + END_LINE;
					Integer startIndex = sbSVG.indexOf(error);

					if (startIndex > 0) {
						toIndex = startIndex + error.length() - 1;
						sbSVG = sbSVG.replace(startIndex, toIndex, "<g>");
					}

					String errorG = "<g><g>";
					Integer startIndexG = sbSVG.indexOf(errorG);

					if (startIndexG > 0) {
						toIndex = startIndexG + errorG.length() - 1;
						sbSVG = sbSVG.replace(startIndexG, toIndex, "<g>");
					}

					undoPointers.remove(undoPointers.size() - 1);

					penOn = false;
					triggerCounter = 0;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		@Override
		public void onUndo() {
			if (undoPointers.size() > 1) {
				sbSVG = sbSVG.replace(
						undoPointers.get(undoPointers.size() - 1),
						sbSVG.length() - 1, "");

				// This resets the path creation sequence
				started = false;

				undoPointers.remove(undoPointers.size() - 1);
				penOn = false;
				triggerCounter = 0;

			}
		}

		@Override
		public void penOn() {
			penOn = true;
			triggerCounter = 0;

		}

		@Override
		public void penOff() {
			penOn = false;

			triggerCounter = 0;
		}
	};

	// Respond to animation finish

	AnimationsContainer.AnimInterface animEnded = new AnimInterface() {

		@Override
		public void onFinish() {
			Intent endIntent = new Intent(DrawActivity.this, Startup.class);
			endIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

			Startup.returning = true;
			triggerCounter = 0;
			exited = true;
			startActivity(endIntent);
			finish();

		}
	};

	// Respond to Reset Click
	ImageButton.OnClickListener onResetClick = new ImageButton.OnClickListener() {

		@Override
		public void onClick(View v) {
			// Erase

			if (!clicked) {
				triggerCounter = 0;
				exited = true;
				clicked = true;
				Bundle args = new Bundle();
				args.putInt("option", 3);
				Intent i = new Intent(DrawActivity.this, PromptDialog.class);
				i.putExtra("args", args);
				DrawActivity.this.startActivityForResult(i, 22);
				
//				//Nothing to Undo, Reset or Save
//				btnSave.setEnabled(false);
//				btnUndo.setEnabled(false);
//				btnReset.setEnabled(false);
			}
		}

	};

	private void resetDrawView() {
		triggerCounter = 0;
		exited = true;
		// clicked = true;

		Intent data = new Intent();
		data.putExtra("option", 3);
		DrawActivity.this.setResult(22, data);
	}

	// check for multiple clicks
	Boolean clicked = false;

	// Respond to Home Click

	ImageButton.OnClickListener onHomeClick = new ImageButton.OnClickListener() {

		@Override
		public void onClick(View v) {
			if (!clicked) {
				triggerCounter = 0;
				exited = true;
				clicked = true;
				Bundle args = new Bundle();
				args.putInt("option", 1);
				Intent i = new Intent(DrawActivity.this, PromptDialog.class);
				i.putExtra("args", args);
				DrawActivity.this.startActivityForResult(i, 22);
			}
		}
	};

	// Respond to Save Click
	ImageButton.OnClickListener onSaveClick = new ImageButton.OnClickListener() {

		@Override
		public void onClick(View v) {
			if (dv.undoCounter > 0) { // Something has been drawn.
				if (!clicked) {
					triggerCounter = 0;
					exited = true;
					clicked = true;
					Bundle args = new Bundle();
					args.putInt("option", 2);
					Intent i = new Intent(DrawActivity.this, SaveDialog.class);
					i.putExtra("args", args);
					DrawActivity.this.startActivityForResult(i, 22);

				}
			}
		}
	};

	ImageButton.OnClickListener onUndoClick = new ImageButton.OnClickListener() {

		@Override
		public void onClick(View v) {
			triggerCounter = 0;
			if (dv.undoCounter > 0) {
				dv.performUndo();
				if (dv.undoCounter == 0) {
					resetDrawView();
					btnSave.setEnabled(false);
					btnUndo.setEnabled(false);
					btnReset.setEnabled(false);
				}
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Integer opt;

		svgout.penOff();

		triggerCounter = 0;
		clicked = false;
		exited = false;

		switch (resultCode) {
		case 5: //
			opt = data.getIntExtra("option", 0);
			response(opt);
			noInputHandler.postDelayed(noInputTask, 5000);
			break;
		case 6: //

			break;
		case 22: {
			opt = data.getIntExtra("option", 0);
			response(opt);
			SoftKeyboard.notRestarting();
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			noInputHandler.postDelayed(noInputTask, 5000);
			break;
		}
		case 99: {
			exited = true;
			finish();
		}

		}

		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	protected void onResume() {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		super.onResume();
	}

	public void response(Integer res) {
		switch (res) {
		case 1:
			clicked = true;
			exited = true;
			finish();
			break;
		case 2:

			clicked = true;
			startAnimation = null;

			break;
		case 3: {
			dv.clear();

			if (undoPointers.size() > 0)
				sbSVG.setLength(undoPointers.get(0));

			undoPointers.clear();
			started = false;
			undoPointers.add(firstUndoMarker);
			clicked = false;
			
			//Nothing to Undo, Reset or Save
			btnSave.setEnabled(false);
			btnUndo.setEnabled(false);
			btnReset.setEnabled(false);
			break;
		}
		case 4: {
			clicked = true;
			exited = true;
			finish();
			triggerCounter = 0;
			break;
		}
		case 5: {
			// this is to catch the cancel buttons and restart the timer
			svgout.penOff();
			clicked = false;
			triggerCounter = 0;
			noInputDialog = false;
		}
		}
	}

	public static StringBuilder getSbSVG() {
		return sbSVG;
	}

}
