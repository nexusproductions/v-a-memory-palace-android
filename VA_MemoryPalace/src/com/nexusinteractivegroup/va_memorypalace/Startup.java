package com.nexusinteractivegroup.va_memorypalace;

import android.os.Bundle;
import android.os.Handler;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewSwitcher.ViewFactory;

public class Startup extends Activity implements ViewFactory
{

	Handler loopHandler;
	ImageSwitcher splashes;
	Integer imageIndex;
	
	ValueAnimator accelAnimator;
	
	ImageView endAnimImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_startup);
				
		imageIndex = 0;
		
		RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.start_layout);
		mainLayout.setOnClickListener(tListener);
		
		splashes = (ImageSwitcher) findViewById(R.id.imageSwitcher1);
		splashes.setFactory(this);
		
		loopHandler = new Handler();
		loopHandler.post(imageLoop);
		
		endAnimImage = (ImageView) findViewById(R.id.errormessage);
	}

	RelativeLayout.OnClickListener tListener = new RelativeLayout.OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			Intent drawIntent = new Intent(Startup.this,DrawActivity.class);
			startActivity(drawIntent);
		}
		
	};
	
	public static Boolean returning = false;
	
	@Override
	protected void onResume()
	{
		if (returning)
		{
			
			returning = false;
			
			endAnimImage.setVisibility(View.VISIBLE);
			
			accelAnimator = ObjectAnimator.ofFloat(endAnimImage, "y", 0f, -1280f).setDuration(1200);
			accelAnimator.setInterpolator(new DecelerateInterpolator(2f));
			
		
			accelAnimator.start();
		}
		super.onResume();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{		
		return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.startup, menu);
		return true;
	}
	
	/*
	 * The image loop for the image front screen // 
	 */
	Runnable imageLoop = new Runnable() 
	{

		@Override
		public void run() 
		{
		
			if (imageIndex == 0)
			{
				splashes.setImageResource(R.drawable.memory_palace_splashscreen1);
				imageIndex = 1;
			}
			else
			{
				splashes.setImageResource(R.drawable.memory_palace_splashscreen2);
				imageIndex = 0;
			}
				
			loopHandler.postDelayed(imageLoop, 15000); // 15 seconds
		}
		
	};

	@Override
	public View makeView() 
	{
		 ImageView i = new ImageView(this);
         i.setBackgroundColor(0xFF000000);
         i.setScaleType(ImageView.ScaleType.FIT_XY);
         i.setLayoutParams(new ImageSwitcher.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
         return i;
	}

}
